{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE QuasiQuotes         #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StrictData          #-}
{-# LANGUAGE TypeApplications    #-}
module Main where
import           Control.Monad.Trans.Resource         (ResourceT, runResourceT)
import           Data.ByteString.Char8                (ByteString)
import           Data.Conduit
import qualified Data.Conduit.List                    as CL
import qualified Data.HashMap.Strict                  as Map
import           Data.Int
import           Data.Vector.Unboxed
import           Data.Wakfu.BinaryData.BinaryDocument
import           Data.Wakfu.BinaryData.Generic        (Reader (..))
import           GHC.Generics                         (Generic)
import           Path

data StaticEffect
    = StaticEffect
    { effectId                     :: Int32,
    actionId                       :: Int32,
    parentId                       :: Int32,
    areaOrderingMethod             :: Int16,
    areaSize                       :: Vector Int32,
    areaShape                      :: Int16,
    emptyCellsAreaOrderingMethod   :: Int16,
    emptyCellsAreaSize             :: Vector Int32,
    emptyCellsAreaShape            :: Int16,
    triggersBeforeComputation      :: Vector Int32,
    triggersBeforeExecution        :: Vector Int32,
    triggersForUnapplication       :: Vector Int32,
    triggersAfterExecution         :: Vector Int32,
    triggersAfterAllExecutions     :: Vector Int32,
    triggersNotRelatedToExecutions :: Vector Int32,
    triggersAdditionnal            :: Vector Int32,
    criticalState                  :: ByteString,
    targetValidator                :: Vector Int64,
    affectedByLocalisation         :: Bool,
    durationBase                   :: Int32,
    durationInc                    :: Float,
    endsAtEndOfTurn                :: Bool,
    isDurationInFullTurns          :: Bool,
    applyDelayBase                 :: Int16,
    applyDelayIncrement            :: Float,
    params                         :: Vector Float,
    probabilityBase                :: Float,
    probabilityInc                 :: Float,
    triggerListenerType            :: Int8,
    triggerTargetType              :: Int8,
    triggerCasterType              :: Int8,
    _31                            :: Int32,
    storeOnSelf                    :: Bool,
    maxExecution                   :: Int16,
    maxExecutionIncr               :: Float,
    maxTargetCount                 :: Int8,
    isFightEffect                  :: Bool,
    hmiAction                      :: ByteString,
    containerMinLevel              :: Int16,
    containerMaxLevel              :: Int16,
    effectCriterion                :: ByteString,
    effectParentType               :: ByteString,
    effectContainerType            :: ByteString,
    dontTriggerAnything            :: Bool,
    isPersonal                     :: Bool,
    isDecursable                   :: Bool,
    notifyInChatForCaster          :: Bool,
    notifyInChatForTarget          :: Bool,
    notifyInChatWithCasterName     :: Bool,
    scriptFileId                   :: Int32,
    durationInCasterTurn           :: Bool,
    effectProperties               :: Vector Int32,
    displayInSpellDescription      :: Bool,
    displayInStateBar              :: Bool,
    recomputeAreaOfEffectDisplay   :: Bool,
    isInt32urnInFight              :: Bool,
    notifyInChat                   :: Bool
    } deriving(Generic, Show)

instance Reader StaticEffect

main :: IO ()
main = do
    print . Map.size =<< runResourceT toMap
    where
        toMap :: ResourceT IO (Map.HashMap Int32 StaticEffect)
        toMap = do
            c <- openDocument [absdir|/path/to/Wakfu/|] 68 (rread @ StaticEffect)
            runConduit $ c .| CL.fold (\acc i -> Map.insert (effectId i) i acc) Map.empty

