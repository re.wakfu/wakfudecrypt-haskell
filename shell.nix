with import <nixpkgs> { };

haskell.lib.buildStackProject {
  name = "haskell-env";
  ghc = haskell.packages.ghc802.ghc;
  buildInputs = [ haskell.packages.ghc802.intero
                  ncurses
                  zlib
                  bzip2
                  cabal-install ];
}
