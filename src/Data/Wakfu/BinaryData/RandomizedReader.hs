{-# LANGUAGE StrictData #-}
{-# LANGUAGE ExplicitForAll #-}
module Data.Wakfu.BinaryData.RandomizedReader
    ( runRandomizedReader
    , RandomizedReader
    , decodeInt8
    , decodeInt16
    , decodeInt32
    , decodeInt64
    , decodeFloat32
    , decodeFloat64
    , decodeBool
    , decodeBytes
    , decodeMap
    , decodeList
    , decodePrefix
    ) where
import           Control.Monad                     (replicateM)
import           Data.Binary.Get
import           Data.Binary.IEEE754               (getFloat32le, getFloat64le)
import           Data.Bits                         (xor)
import           Data.ByteString                   (ByteString)
import           Data.ByteString.Lazy              (toStrict)
import           Data.Conduit                      (ConduitT, Void)
import qualified Data.Conduit.Binary               as CB
import           Data.Conduit.Serialization.Binary (sinkGet)
import           Data.Int
import Data.STRef.Strict
import Control.Monad.ST.Strict
import Control.Monad.IO.Class

data RandomState =
    RandomState { _position :: Int
                , seed      :: Int8
                , _mul      :: Int
                , _add      :: Int
                }

type RandomizedReader a = STRef RealWorld RandomState -> ConduitT ByteString Void IO a

runRandomizedReader :: (STRef RealWorld RandomState -> ConduitT ByteString o IO a)
                    -> Int
                    -> Int
                    -> ConduitT ByteString o IO a
runRandomizedReader m mul add = do
    ref <- liftIO . stToIO . newSTRef $ RandomState 4 (fromIntegral $ mul `xor` add) mul add
    m ref

decodeInt8 :: RandomizedReader Int8
{-# INLINE decodeInt8 #-}
decodeInt8 = decodeIntegral 1 getInt8

decodeInt16 :: RandomizedReader Int16
{-# INLINE decodeInt16 #-}
decodeInt16 = decodeIntegral 2 getInt16le

decodeInt32 :: RandomizedReader Int32
{-# INLINE decodeInt32 #-}
decodeInt32 = decodeIntegral 4 getInt32le

decodeInt64 :: RandomizedReader Int64
{-# INLINE decodeInt64 #-}
decodeInt64 = decodeIntegral 8 getInt64le

decodeFloat32 :: RandomizedReader Float
{-# INLINE decodeFloat32 #-}
decodeFloat32 = decode 4 getFloat32le

decodeFloat64 :: RandomizedReader Double
{-# INLINE decodeFloat64 #-}
decodeFloat64 = decode 8 getFloat64le

decodeBool :: RandomizedReader Bool
{-# INLINE decodeBool #-}
decodeBool = fmap (/= 0) . decodeInt8

decodeBytes :: RandomizedReader ByteString
decodeBytes ref = do
    len <- decodePrefix ref
    _ <- liftIO . stToIO $ advance len ref
    bs <- CB.take len
    return $ toStrict bs

decodeList :: ConduitT ByteString Void IO a -> RandomizedReader [a]
decodeList mr ref = decodePrefix ref >>= flip replicateM mr

decodeMap :: ConduitT ByteString Void IO k
          -> ConduitT ByteString Void IO v
          -> RandomizedReader [(k, v)]
decodeMap kr vr ref = decodePrefix ref >>= flip replicateM ((,) <$> kr <*> vr)

updateState :: forall s. Int -> STRef s RandomState -> ST s ()
updateState n ref =
    modifySTRef' ref $ \(RandomState pos s mul add) ->
        RandomState (pos + n) (s + fromIntegral (mul * pos + add)) mul add

decodeIntegral :: (Integral a) => Int -> Get a -> RandomizedReader a
decodeIntegral n getm ref = do
    v <- decode n getm ref
    s <- liftIO . stToIO $ readSTRef ref
    return $ v - (fromIntegral $ seed s)

advance :: Int -> STRef s RandomState -> ST s ()
{-# INLINE advance #-}
advance n ref = modifySTRef' ref $ \(RandomState pos s mul add) -> RandomState (pos + n) s mul add

decode :: Int -> Get a -> RandomizedReader a
{-# INLINE decode #-}
decode n getm ref = liftIO (stToIO $ updateState n ref) >> sinkGet getm

decodePrefix :: RandomizedReader Int
{-# INLINE decodePrefix #-}
decodePrefix = fmap fromIntegral . decodeInt32
