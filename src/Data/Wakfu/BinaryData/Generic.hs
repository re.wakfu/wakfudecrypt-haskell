{-# LANGUAGE DefaultSignatures #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE TypeOperators     #-}
module Data.Wakfu.BinaryData.Generic(Reader(..)) where
import           Data.ByteString                        (ByteString)
import           Data.Hashable                          (Hashable)
import qualified Data.HashMap.Strict                    as Map
import           Data.Int
import qualified Data.Vector                            as V
import qualified Data.Vector.Unboxed                    as UV
import           Data.Wakfu.BinaryData.RandomizedReader
import           GHC.Generics

class Reader a where
    rread :: RandomizedReader a

    default rread :: (Generic a, GReader (Rep a)) => RandomizedReader a
    rread = fmap to . gread

class GReader f where
    gread :: RandomizedReader (f a)

instance GReader U1 where
    gread _ = pure U1

instance (GReader a, GReader b) => GReader (a :*: b) where
    gread ref = (:*:) <$> gread ref <*> gread ref

-- instance (GReader a, GReader b) => GReader (a :+: b) where
--   gread = undefined

instance GReader a => GReader (M1 D c a) where
    gread = fmap M1 . gread

instance GReader a => GReader (M1 C c a) where
    gread = fmap M1 . gread

instance GReader a => GReader (M1 S s a) where
    gread = fmap M1 . gread

instance Reader a => GReader (K1 i a) where
    gread = fmap K1 . rread

-- instances for the usual types

instance Reader Int8 where
    rread = decodeInt8

instance Reader Int16 where
    rread = decodeInt16

instance Reader Int32 where
    rread = decodeInt32

instance Reader Int64 where
    rread = decodeInt64

instance Reader Float where
    rread = decodeFloat32

instance Reader Double where
    rread = decodeFloat64

instance Reader Bool where
    rread = decodeBool

instance Reader a => Reader [a] where
    rread ref = decodeList (rread ref) ref

instance (Reader k, Reader v, Eq k, Hashable k) => Reader (Map.HashMap k v) where
    rread ref = Map.fromList <$> decodeMap (rread ref) (rread ref) ref

instance (Reader a, UV.Unbox a) => Reader (UV.Vector a) where
    rread ref = decodePrefix ref >>= flip UV.generateM (const $ rread ref)

instance Reader a => Reader (V.Vector a) where
    rread ref = decodePrefix ref >>= flip V.generateM (const $ rread ref)

instance Reader ByteString where
    rread = decodeBytes
