{-# LANGUAGE StrictData #-}
{-# LANGUAGE ExplicitForAll #-}
module Data.Wakfu.BinaryData.BinaryDocument(openDocument) where
import qualified Codec.Archive.Zip                      as Zip
import           Control.Monad                          (replicateM_)
import           Control.Monad.Trans.Resource           (ResourceT)
import           Data.Binary.Get                        (getInt32le)
import           Data.Conduit
import qualified Data.Conduit.List                      as CL
import           Data.Conduit.Serialization.Binary      (sinkGet)
import           Data.Int
import           Data.Monoid                            ((<>))
import           Data.Wakfu.BinaryData.RandomizedReader
import           Path
import           Control.Monad.IO.Class

data Entry =
    Entry { _entryId       :: Int64
          , _entryPosition :: Int32
          , _entrySize     :: Int32
          , _entrySeed     :: Int8
          }

magicNumber :: (Num a) => a
magicNumber = 756423

openDocument :: forall a b. Path b Dir
             -> Int
             -> RandomizedReader a
             -> ResourceT IO (ConduitT () a (ResourceT IO) ())
openDocument gamePath idx reader = do
    rf <- parseRelFile ("game/contents/bdata/" <> show idx <> ".jar")
    let path = gamePath </> rf
    src <- Zip.withArchive (toFilePath path) $ Zip.getEntrySource =<< Zip.mkEntrySelector (show idx <> ".bin")
    (src', version) <- src $$+ readVersion
    (src'', entryCount) <- src' $$++ (transPipe liftIO $ runRandomizedReader readHeader idx version)
    (src''', version') <- src'' $$++ readVersion
    let readAll = \ref -> CL.sequence (toConsumer $ reader ref) .| CL.isolate (fromIntegral entryCount)
    return $ unsealConduitT src''' .| (transPipe liftIO $ runRandomizedReader readAll idx version')
    where
        readVersion = fromIntegral . (+ magicNumber) <$> sinkGet getInt32le

readHeader :: RandomizedReader Int32
readHeader ref = do
    entryCount <- decodeInt32 ref
    _ <- replicateM_ (fromIntegral entryCount) $
        Entry <$>
            decodeInt64 ref <*>
            decodeInt32 ref <*>
            decodeInt32 ref <*>
            decodeInt8 ref
    idxCount <- decodeInt8 ref
    _ <- replicateM_ (fromIntegral idxCount) $ do
        unique <- decodeBool ref
        _ <- decodeBytes ref
        cnt <- decodeInt32 ref
        replicateM_ (fromIntegral cnt) $ do
            _ <- decodeInt64 ref
            if unique then do
                _ <- decodeInt32 ref
                return ()
            else do
                _ <- decodeList (decodeInt32 ref) ref
                return ()
    return entryCount
